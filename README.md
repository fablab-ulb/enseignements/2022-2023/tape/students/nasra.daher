# Journal de bord de Nasra Daher

## Qui suis-je ? 

Je poursuis actuellement mes études en master de sciences physiques à l'Université libre de Bruxelles. Dans ce cadre, je suis inscrite au cours intitulé "Techniques avancées de physique expérimentale" dont une partie porte sur la science frugale. Cette dernère est une science expérimentale qui consiste à résoudre un problème scientifique avec les méthodes les plus simples matériellement et les plus efficaces. Pour cette partie du cours, nous devons participer à une question de recherche d'actualité. Cela m'intéresse car en plus de la formation à la recherche, le projet présente un réel impact. 

## Projet 

L'idée est de participer à un projet qui débute cette année : surveiller la pollution de l'eau dans la baie de La Havane à l'aide de bouées frugales. Nous devons étudier un microscope optique innovant : le Foldscope. Celui-ci a été créé par le physicien Manu Prankash et son étudiant Jim Cybulski de l'Université de Standford. L'inspiration leur est venue lors de multiples voyages de Cybulski dans les stations de recherche de pays en développement où le manque de microscopes utilisables est fréquent. Leur ambition était de créer le meilleur microscope pour moins de 1$. C'est un bon exemple de science frugale et il est donc naturel que notre attention se soit portée sur lui. 








Ce projet s'inscrit dans le cadre d'une aide à Cuba octroyée par l'Académie de recherche et d'enseignement supérieur (ARES) qui finance des [collaborations](https://www.ares-ac.be/fr/cooperation-au-developpement)  entre des établissements de l'enseignement supérieur de pays en développement et des établissements de la fédération Wallonie-Bruxelles. 

## 26 septembre 2022

Pour nous présenter le Foldscope, nous avons regardé une [conférence TED](https://www.youtube.com/watch?v=h8cF5QPPmWU) donnée par Manu Prakash. J'ai trouvé cette vidéo très chouette. On peut comprendre grâce aux explications et démonstrations pourquoi ce microscope est un objet tout à fait remarquable.  

<div align='center'> 

[![](http://img.youtube.com/vi/h8cF5QPPmWU/0.jpg)](https://www.youtube.com/watch?v=h8cF5QPPmWU)

</div>

Nous avons également lu un [article](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0098781) de Cybulski, Clements et Prakash décrivant le Foldscope puis nous avons chacun mentionné 3 divers aspects techniques qui nous avaient frappés. Voici ceux que j'ai relevés ainsi que les raisons pourquoi :

**Versatilité :** Le microscope fonctionne, en mode observation, par illumination en champ clair (l'échantillon est illuminé par l'arrière et apparaît plus sombre sur un fond clair à cause de l'atténuation de la lumière lorsqu'elle le traverse). Cependant, il est possible de le modifier pour faire de la microscopie en champ sombre et à fluorescence. J'ai trouvé cela très intéressant car cela permet d'observer une plus grande variété d'échantillons. Je suis notamment intéréssée par la possibilité d'utiliser ces techniques pour observer du phytoplancton ou des microplastiques dans le but de surveiller la qualité de l'eau. 

**Possibilité de combiner plusieurs techniques de microscopie :** En disposant des lentilles en réseau dans le Foldscope, il est possible de créer plusieurs chemins optiques différents. Ainsi, pour les échantillons non-continus, on peut augmenter le champ de vision en superposant un certain nombre de petits champs de vision. Ce qui m'avait le plus interpellée était la possibilité d'implémenter plusieurs techniques d'illumination à la fois. Par exemple, en combinant les microscopies en champ clair et à fluorescence on pourrait repérer des marquages fluorescents et ensuite identifier les structures non-fluorescentes aux alentours. J'ai pensé de nouveau pensé à l'utilité pour déterminer la qualité de l'eau, notamment le phytoplancton fluorescent. Cela a aussi une utilité dans le domaine médical où des substances fluorescentes sont utilisées pour détecter des pathogènes, des cellules cancéreuses ou des anomalies des tissus biologiques par [immunofluorescence](https://fr.wikipedia.org/wiki/Immunofluorescence). 

**Résolution de l'ordre du micromètre :** Le Foldscope possède une résolution qui est suffisamment bonne que pour l'utiliser afin d'observer les détails de petits objets. De plus, celui-ci peut même atteindre une résolution submicronique. J'ai trouvé cet aspect remarquable et utile pour l'observation du microplastique. 

<div align='center'> ![article](img/article.png) 

*Photos prises avec le Foldscope dans plusieurs configurations ainsi qu'un schéma les représentant.*
</div>

La lecture de cet article ne fût pas facile. J'ai du le relire plusieurs fois pour mieux comprendre car celui-ci est plutôt technique. Néanmoins j'ai pu, grâce à notre discussion et aux aspects relevés par les autres étudiants, en retirer l'essentiel.  


Nous avons également été initiés à l'utilisation de GitLab, notamment à la création et à l'édition d'un ficher [Markdown](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown). Nous avons encore appris comment réduire la taille d'un fichier image avec le logiciel [GIMP](https://www.gimp.org/about/).  

Enfin, nous avons manipulé le microscope pour mieux en comprendre le montage. Moi et mon camarade [Alexandre](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/alexandre.hallemans) avons démonté puis monté un Foldscope déjà fonctionnel. Grâce aux instructions incluses dans la boîte de rangement et le fait que celui-ci était déjà monté, nous avons pu faire le montage sans trop de difficultés. C'était chouette de voir comment le pliage origami permettait d'emboîter les diverses parties du microscope ensemble. 


Nous avons ensuite tenté d'observer un échantillon contenant une tique mais sans succès. 
<div align='center'> ![foldscope partiellement démonté](img/foldscope1.jpg) ![foldscope monté](img/foldscope2.jpg) </div>

## 3 octobre 2022 

Nous nous sommes interessés aux applications qui avaient été faites à l'aide d'un Foldscope. En effet, il y a déjà un certain nombre de publications scientifiques qui ont en fait l'utilisation. Certaines se trouvent sur le [site web](https://foldscope.com/pages/publications) du Foldscope. Les applications ne se limitent pas au monde académique, il existe une plateforme appellée [Microcosmos](https://microcosmos.foldscope.com/explore) sur laquelle n'importe qui avec un Foldscope peut tenir un carnet de laboratoire pour documenter leurs projets. L'accent est mis sur le caractère ouvert de la plateforme. Toutes les publications sont publiques, ce qui permet aux personnes désireuses d'explorer les possibilités de l'univers microscopique.   

### Exemples d'applications techniques du Foldscope : 

J'ai trouvé trois applications du Foldscope dans la littérature scientifique en cherchant le mot-clé "Foldscope" sur [Google Scholar](https://scholar.google.com/). Il y avait beaucoup d'exemples dans le domaine médical c'est pourquoi j'en ai inclus deux dans des domaines non-médicaux.

**1. Identification rapide et peu coûteuse du safran de contrefaçon à l'aide d'un Foldscope et de la technologie de l'apprentissage automatique.**

Le safran est une épice rare et c'est aussi la plus chère au monde. Elle est donc malheureusement sujette à l'adultération (l'ajout d'un produit de moindre qualité) voire la contrefaçon par des  faudreurs. La pratique de fraude la plus fréquemment utilisée est la coloration artificielle d'autres plantes pour leur donner l'apparence du safran. Les méthodes actuelles pour détecter le faux safran sont compliquées, onéreuses et nécessitent une certaine expertise. En vue de [l'augmentation](https://www.currentscience.ac.in/Volumes/104/06/0686.pdf) de ce type de fraude, il est important de trouver des techniques plus rapides et accessibles pour les vendeurs et les consommateurs. Les auteurs de cette [étude](https://www.frontiersin.org/articles/10.3389/fpls.2022.945291/full) ont testé deux méthodes pour identifier le faux safran sous forme de filaments avec un smartphone attaché à un Foldscope. La première utilise le microscope en combinaison avec une technique de coloration ou de décoloration afin de créer une affiche imprimée qui pourra être utilisée comme référence pour une comparaison visuelle. La deuxième combine le microscope avec l'utilisation d'un modèle d'apprentissage automatique à l'aide d'une application mobile. 


<div align='center'> ![photos de safran](img/saf.jpg)

*Affiche pour l'identification du safran authentique : Safran véritable en filaments (a-d), safran véritable en filaments coupés (e-h); faux échantillons (i-##).*

![photos et montage du Foldscope](img/saf2.JPEG)

*Pollen sur du safran fraîchement coupé (a); divers échantillons (b-e); échantillon de safran monté sur un Foldscope (f).* </div>

Les deux méthodes sont efficaces. L'apprentissage automatique combiné avec le Foldscope permet de rapidement et efficacement identifier le faux safran. Cette application est particulièrement intéressante car elle montre qu'il est possible d'utiliser le Foldscope à des fins pratiques et accessibles au public.


**2. Calibration et validité d'un Foldscope pour le suivi des caractéristiques du pollen comme outil de dépistage de la tolérance à la sécheresse.** 


La pollinisation est cruciale pour les récoltes agricoles. Il est donc important de déterminer diverses caractéristiques (viabilité, adhérence, germination, croissance des tubes et longévité) du pollen en vue d'identifier des génotypes qui possèdent une plus grande efficacité de pollinisation et une meilleure résistance au stress. Actuellement, les chercheurs utilisent des microscope optiques composés pour étudier les traits *in vitro* du pollen mais ceux-ci ne sont pas adaptés pour une étude *in vivo*, ce qui limite la quantité d'informations obtenue sur la pollinisation des cultures car une étude en temps réel sur le champ est plus complète. L'incapacité d'utiliser des microscopes composés dans des conditions de terrain pourrait être surmontée en utilisant des microscopes portables tels que le Foldscope. Les auteurs de [cet article](https://www.sciencedirect.com/science/article/abs/pii/S0254629922002563?via%3Dihub) souhaitent déterminer la calibration et la validité du Foldscope pour l'étude *in vitro* du pollen en le comparant avec un microscope composé dans l'optique future de pouvoir l'utiliser dans des études *in vivo*. 

<div align='center'> ![comparaison Foldscope et microscope classique](img/pollen1.jpg) 

*Images de pollen germé prises avec le Foldscope (a, c) et le microscope composé (b, d), germination du pollen après 3h calculée à partir des images prises par le Foldscope (en gris clair) et par le microscope composé (en gris foncé) (e).* 

![suivi croissance de tubes de pollen](img/pollen2.jpg) 

*Images de la croissance de tubes de pollen prises avec le Foldscope* </div>

Le Foldscope fournit des images équivalentes au microscope composé. C'est donc une bonne alternative et il peut être utilisé pour des études sur le terrain.

**2. Dépistage innovant et analyse de la sensibilité au traitement de *Candida albicans* à l'aide d'un Foldscope.**

*Candida albicans* est une espèce de levure que l'on trouve dans la flore instestinale d'un grand pourcentage de la population humaine. Elle est habituellement commensale (bénéfique pour la levure mais neutre pour l'hôte) mais peut devenir pathologique et provoquer une infection appelée la candidose. Beaucoup de personnes immuno-déprimées vivent dans des régions sans microscope où une cette pathologie peut être mortelle. Les auteurs de [cette étude](https://link.springer.com/article/10.1007/s12210-021-00974-6) ont tenté de déterminer l'utilité d'un Foldscope pour diagnostiquer la candidose et étudier l'effet d'un traitement médicamental sur la levure. 

<div align='center'> ![cellules de candida albicans](img/cand0.jpg) 

*Images de cellules de Candida Albicans prises avec le Foldscope*

![suivi croissance de tubes de pollen](img/cand1.jpg) 

*Effets inhibitoires de l'antifongique sur la croissance de Candida Albicans* </div>

Le Foldscope est un bon choix de microscope pour diagnostiquer et surveiller l'évolution d'une infection à *Candida albicans* dans les zones qui manquent de matériel médical. 

### Observations personnelles : 

Il avait été demandé à la fin de cette séance de faire une observation. Heureusement, [Alexandre](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/alexandre.hallemans) a trouvé la raison pour laquelle nous n'arrivions pas à observer la tique. Nous avions oublié de coller la lentille à son coupleur magnétique de sorte que celle-ci se détachait à cause du coupleur magnétique de l'ouverture lorsqu'on dépliait l'arrière du microscope.  

Nous avons pris ensemble deux images d'échantillons qui se trouvaient déjà dans la boîte du Foldscope avec nos téléphones. L'un a servi d'objectif et l'autre de source de lumière car la LED fournie avec le microscope ne fonctionnait pas. 

<div align='center'> ![échantillon 1](img/test1.jpg) 
![lame](img/lametest1.jpg) 

*Rhizome (tige sous-terraine servant de réserve alimentaire) de fougère*

![échantillon 2](img/test2.jpg) 
![lame](img/lametest2.jpg) 

*Ixodida (tique)* </div>

Il est aussi intéressant d'observer des objets du quotidien. J'ai décidé, pour commencer, de mesurer l'épaisseur d'un de mes cheveux. Pour ce faire, j'ai pris une lamelle en verre et j'ai collé un bout de scotch que je peux enlever pour réutiliser la lamelle pour un autre échantillon. J'ai ensuite placé un morceau de cheveu sur la surface non-collante du scotch avant de placer un autre bout pour protéger l'échantillon. 

<div align='center'> 
![grille](img/grid.jpg)  
![cheveu](img/hair.jpg) 

*Grille de référence et cheveu* 
</div>

Afin de mesurer l'épaisseur, j'ai utilisé comme échelle de référence une grille de carrés de 0,5mm de longueur fournie avec le microscope que l'on peut coller sur une lamelle en verre. Une première façon de procéder, inspirée par cet [article](https://www.mdpi.com/2071-1050/14/20/13427/pdf), est d'utiliser la longueur connue d'un carré pour mesurer le diamètre du champ de vision obtenu avec le Foldscope monté sur un téléphone à l'aide d'une application mobile qui permet d'annoter des longueurs sur des images. On peut ensuite utiliser la taille du champ de vision pour mesurer sur la photo de l'échantillon. Une deuxième façon est de coller l'échantillon sur la lame avec la grille et de mesurer grâce à celle-ci la largeur du cheveu avec l'application au lieu d'utiliser le champ de vision comme référence. J'ai utilisé la version gratuite de [ImageMeter](https://imagemeter.com/) disponible pour les appareils Android. On peut aussi utiliser le logiciel open-source [ImageJ](https://imagej.net/) disponible sur PC. J'ai obtenu des résultats similaires pour les deux méthodes. En comparant avec la [littérature](https://hypertextbook.com/facts/1999/BrianLey.shtml), la valeur de 0,1 mm (100 $\mu m$) que j'ai trouvée semble raisonnable. 

<div align='center'> 
![grille](img/scale.jpg)  
![cheveu](img/hair_m.jpg) 
![cheveu2](img/hair_m2.jpg) 

*Largeur du champ de vision approximative : 1,1 mm. Largeur du cheveu : 0,1mm.* 
</div>  

## 10 octobre 2022 

Nous avons rediscuté des aspects techniques qui nous avez frappés afin que chacun choisisse une direction à explorer. Pour ma part, j'ai rementionné les techniques de microscopie. Ensuite, nous avons passé le reste du cours à manipuler le Foldscope pour mieux en comprendre le fonctionnement. Avec l'aide du professeur, j'ai essayé de capturer des images grâce à un appareil photo reflex numérique. La tâche fut plus compliquée qu'avec un téléphone. En effet, il était difficile d'obtenir une image nette car l'objectif n'étant pas directement relié au microscope par un aimant, l'alignement était moins stable.


<div align='center'> 
![appareil photo](img/camera1.jpg)  
![observation](img/camera2.jpg)

*Montage de l'appareil photo reflex avec le Foldscope et observation de [l'échantillon de rhizome](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/nasra.daher/-/tree/nasra.daher-main-patch-50200#observations-personnelles-). Comme on peut le voir, l'image est floue.* 
</div>

Au terme de ce cours, chacun a choisi une mission à accomplir pour le prochain. J'ai choisi d'explorer les techniques de bright field et de dark field tant sur le plan théorique que sur le plan pratique. 

## 17 octobre 2022

Un microscope optique permet de grossir l’image d’objets de petites dimensions. Dans sa version la plus simple, il est constitué d’une source de lumière visible qui illumine l’échantillon et d’une lentille convergente qui forme une image virtuelle droite agrandie par grossissement optique. Cette image virtuelle est formée par l'intersection de l'extension imaginaire des rayons divergents qui sortent de la lentille. 


<div align='center'> 
![diagramme microscope simple](img/virtual.png) 

*Formation de l'image virtuelle.* [Source de l'image.](http://www.educonline.net/spip/spip.php?article258&lang=fr)

![diagramme microscope simple](img/simple_diagram.png) 

*Grossissement de l'objet par la lentille. L'objet est vu à l'oeil nu avec un angle $\beta$ mais parait avoir un angle $\alpha$ à travers le microscope. Le grossissement optique est alors $G = \frac{\alpha}{\beta}$.* [Source de l'image, modifiée.](https://commons.wikimedia.org/wiki/File:Microscope_simple_diagram.png#/media/File:Microscope_simple_diagram.png)
</div>

Dans le microscope, les ondes lumineuses sont déformées (diffraction) par l'échantillon et l'ouverture de la lentille avant de former une image (figure de diffraction) par interférences constructives et destructives entre les ondes (résultant en des maxima et des minima d'intensité pour la figure de diffraction). Pour une lentille avec une ouverture circulaire, l'image d'un point lumineux dans le microscope est donc une figure de diffraction dont le premier maximum est appelé *disque ou tache d'Airy*. 


Il y a une limite théorique pour la résolution; deux points sont spatialement résolubles si le premier maximum de diffraction du premier point coïncide avec le premier minimum de diffraction du second point. C'est le critère de Rayleigh, définissant la séparation angulaire minimale entre deux points adjacents, qui est : $\theta \approx \frac{1,22 \lambda}{d}$ où $\theta$ est l'angle (en radians) entre l'axe central de l'ouverture circulaire et l'axe allant de son centre vers le minimum de diffraction, $d$ est le diamètre de l'ouverture circulaire et $\lambda$ la longueur d'onde. 

<div align='center'> 
![disque d'Airy](img/airy_disk.png) 
![disque d'Airy](img/angular_resolution.png) 

*La distance angulaire entre le premier maximum et le premier minimum est $\frac{1,22 \lambda}{D}$, ce qui donne la séparation minimale selon le critère de Rayleigh.* Source de l'image [1](https://www.gatinel.com/glossaire/airy-tache-d/) et [2](http://electron9.phys.utk.edu/optics421/modules/m5/Diffraction.htm).
</div>

### Microscopie bright-field :

Une des techniques d'illumination les plus basiques en microscopie est la microscopie optique en champ clair. Concrètement, l’objet est illuminé par le bas avec une source de lumière et les zones plus ou moins denses atténuent par absorption la lumière qui est ensuite collectée par la lentille objectif, ce qui permet d’observer l’objet qui est sombre sur un fond plus clair. Pour les échantillons transparents, il est difficile de déceler les détails car le contraste est trop faible. En effet, ce type d'échantillon induit des changements de phases qui sont invisibles à l'oeil nu et il n'y a pas assez de différence de luminosité avec le fond. Pour améliorer le contraste, on peut utiliser des filtres optiques ou colorer artificiellement l’échantillon. Un autre moyen est d’utiliser une lentille condensatrice avec une ouverture variable entre l’objet et la source de lumière. Le contraste augmente en réduisant la taille de l'ouverture. Il y a plusieurs raisons à cela : 

1. Moins de lumière parasite réfléchie et diffusée à périphérie de la lentille qui vient obstruer l'image de l'objet. 

2. Plus de cohérence des rayons lumineux car on se limite à une portion de la lumière incidente, ce qui veut dire que les différences de phases sont mieux définies. Les interférences, qui dépendent des déphasages, résultent alors en de plus grandes différences d'amplitude et donc en plus de contraste. 

Cette amélioration se fait aux dépens de la résolution spatiale car celle-ci dépend du diamètre de l'ouverture (critère de Rayleigh) mais il est possible de trouver un équilibre entre résolution et contraste.

### Microscopie dark-field :

Une autre technique est la microscopie optique en champ sombre. La configuration est similaire à celle de la microscopie optique en champ clair avec lentille condensatrice sauf que, contrairement à celle-ci, la lumière incidente directe est bloquée par un filtre en forme de disque ce qui crée un cône de lumière. Les rayons de ce cône sont focalisés par la lentille condensatrice vers l'échantillon. Celui-ci va diffuser, réfracter et diffracter les rayons. La partie du cône de lumière qui passe sans intéragir avec l'échantillon est bloquée par l'ouverture de la lentille objectif car son angle d'ouverture est plus grand. Seule la lumière déviée par l'échantillon est collectée lors de la formation de l'image. On observe alors un objet clair sur un fond sombre. Cette technique est utile pour observer des spécimens transparents. Cette technique est utile pour pouvoir observer les détails d'échantillons peu diffracteurs 

<div align='center'> ![diagramme dark field](img/dark_field_diagram.png) 

*Diagramme illustrant le chemin optique en microscopie dark field.* 
[Source de l'image.](https://commons.wikimedia.org/wiki/File:Dark_Field_Microscope.png#/media/File:Dark_Field_Microscope.png)
</div>

**Références**

[Bright-field microscopy, Wikipédia.](https://en.wikipedia.org/wiki/Bright-field_microscopy)

[Dark-field microscopy, Wikipédia.](https://en.wikipedia.org/wiki/Dark-field_microscopy)

Douglas B. Murphy, Michael W. Davidson*, Fundamentals of Light Microscopy and Electronic Imaging, Second Edition. 

J'ai repris [une méthode trouvée sur le site Microscosmos](https://microcosmos.foldscope.com/?p=7042) pour tenter de produire des images dark field. Pour ce faire, j'ai utilisé une lentille condensatrice 230x incluse dans la boîte du Foldscope et un marqueur noir pour dessiner un disque sur un morceau de plastique qui va servir de filtre pour bloquer la lumière au centre de la lentille. La taille du disque est d'environ 2/3 la taille de la lentille. Il est préfèrable de noircir le disque en faisant des. 

J'ai trouvé que la méthode avec un marqueur n'était pas parfaite. En effet, celui-ci ne bloque pas totalement la lumière ce qui crée une lueur bleue qui nuie à la qualité des images.  
 
<div align='center'> ![photo filtre marqueur](img/dot.jpg) 
![test avec marqueur](img/dot_test.jpg) 
</div>


## 24 octobre 2022

En discutant de mon progrès avec le professeur, j'ai du préciser mon objectif qui était un peu trop large afin de pouvoir obtenir quelque chose de concret à la fin de cette partie du cours. J'ai décidé d'essayer d'observer un même objet en bright-field et en dark-field. 
À cet fin, j'ai choisi de découper au laser des filtres dark-field dans du carton noir avec l'aide du professeur. Je me suis inspirée par ce qui avait été déjà fait par Cibulski et al et aussi par un [utilisateur de Microcosmos](https://microcosmos.foldscope.com/?p=9060) qui avait également préféré utiliser du papier après avoir essayé la méthode avec le marqueur. J'ai repris les dimensions utilisées dans l'article.  

<div align='center'> ![filtre dark field en papier](img/paper_filter_article.png) 
![filtre dark field en papier](img/paper_filter_micro.jpg) 
</div>

Nous avons utilisé un logiciel de dessin vectoriel appelé [CorelDRAW](https://www.coreldraw.com/en/?link=wm). Pour le premier essai, nous avons dessiné un rectangle de mêmes dimensions que le coupleur magnétique sur lequel la lentille est collée ainsi qu'un cercle d'un de rayon 2 mm centré au même endroit que le trou pour la lentille sur le coupleur. Au sein de ce cercle, nous avons dessiné un disque d'un rayon de 1,5 mm relié au cercle par 3 tiges. La découpeuse avait pour réglages: vector job, auto-focus, paper profile avec 100% de vitesse et 15% de puissance. La puissance du laser était trop faible donc nous avons du couper deux fois. J'ai ensuite essayé le filtre avec mon Foldscope. Malheureusement, je me suis rendue compte que celui-ci ne bloquait pas assez de lumière car on voyait un disque lumineux alors qu'il n'y avait pas d'échantillon. J'ai donc décidé d'en refaire avec des rayons intérieurs plus grands : 1,75, 2 et 2,25 mm. La tâche fut compliquée car il fallait prendre en main le logiciel et réussir à obtenir la bonne figure avec certaines manipulations. 


## 28 octobre 2022

Je suis retournée au Fablab pour refaire des filtres plus grands et obtenir une meilleure atténuation de la lumière venant du fond. J'ai réutilisé CorelDRAW pour créer une série de filtres avec des rayons allant de 2,5 à 3mm par pas de 0,25mm.

<div align='center'> ![filtres coupés](img/filters.jpg) 

*Filtres en carton noir, de rayon intérieur allant de 1,5 mm à 3mm par pas 0,25mm (de gauche à droite). L'un d'entre eux est monté sur la lentille.*

</div>

Après plusieurs essais sans succès avec la première lentille, j'ai essayé d'utiliser une deuxième lentille 140X venant d'un autre Foldscope comme lentille condensatrice. J'ai procédé de la même façon qu'avec la première lentille, c'est-à-dire en utilisant le filtre le plus petit et en augmentant la taille jusqu'à ce que j'aie une atténuation satisfaisante de la lumière. Malheureusement, je n'obtiens toujours pas d'images en dark field. Les images obtenues semblent s'en rapprocher mais j'arrive à obtenir un résultat similaire même après avoir enlevé la lentille condensatrice (en mode bright-field donc). En fait, la LED fournie n'est pas assez puissante que pour illuminer l'échantillon une fois que le filtre est monté. J'utilisais alors la lampe d'un téléphone, plus puissante. L'alignement avec cette source de lumière était plus compliqué. Il est possible que ces images aient été obtenues par illumination oblique : l'objet est illuminé par une seule direction seulement, ce qui crée des déphasages qui révèlent certains détails qui étaient invisibles auparavant. C'est une technique qui a son utilité mais qui n'est pas celle que j'essayais d'appliquer. 

### Tentative d'observation en dark-field :

<div align='center'> !["dark-field"](img/dark_test.jpg) 
![bright-field](img/bright_test.jpg) 

*À gauche : image d'une tique en "dark-field". À droite : même tique en bright-field.* 

</div>

### Tentative d'observation en bright-field avec coloration :

Comme je n'ai pas réussi à avoir une observation en dark-field, j'ai souhaité tester une des méthodes pour améliorer le contraste en bright-field : la coloration artificielle de l'échantillon. J'ai prélevé un morceau de la petite peau transparente présente entre les couches internes d'un oignon et je l'ai coloré avec un mélange d'encre bleu et de vinaigre qui est une [méthode simple et efficace](https://journals.asm.org/doi/full/10.1128/aem.64.12.5004-5007.1998). J'étais plutôt contente du résultat final car non seulement les membranes cellulaires étaient bien visibles, les noyaux l'étaient aussi. Résultat concluant ! 

<div align='center'> ![coloration oignon](img/stained_onion1.jpg) 
![oignon coloré](img/stained_onion2.jpg) 

*Coloration de l'onion et résultat.* 

</div>

<div align='center'> ![oignon non-coloré](img/unstained_onionn.jpg) 
![oignon coloré](img/stained_onion3.jpg) 

*À gauche : oignon non-coloré, les membranes cellulaires sont visibles. À droite : oignon coloré; on peut maintenant voir les noyaux des cellules.* 

</div>

## Conclusion 

Au terme de ce cours, j'ai pu apprendre un certain nombre de choses sur la microscopie même si ce n'est que le sommet d'un iceberg pour un domaine aussi vaste. Mon premier défi qui était de pouvoir observer un même échantillon à la fois bright-field et dark-field n'a pas pu être relevé.Néanmoins, d'autres personnes ont pu le faire et j'encourage vivement les utilisateurs du Foldscope ou autres amateurs de microscopies low-cost d'essayer d'en exploiter les possibilités. Le Foldscope est vraiment un outil qui a sa place dans le monde de la microscopie, amateure ou même professionnelle. 

